const { Sequelize } = require('sequelize')
const mysql = require('mysql2')

const db = new Sequelize('PassportAuth', 'root', '', {
    dialect: 'mysql',
    host: 'http://172.16.144.226:82/',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

module.exports = db;
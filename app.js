const express = require('express');
const app = express();
const passport = require('passport')
const session = require('express-session')
const db = require('./dbconnect');
const route = require('./router');
const router = require('./router');
const MySQLStore = require('express-mysql-session')(session)

// passport config
require('./controller/passportconfig')(passport)


app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(session({
    secret: 'keyboard cat',
    store: new MySQLStore({
        host: 'localhost',
        user: 'root',
        database: "PassportAuth",
        password: "123123"
    }),
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24, httpOnly: true
    }
}))


app.use(passport.initialize());
app.use(passport.session());



app.set('views', 'views')
app.set('view engine', 'ejs')

db.authenticate().then(() => {
    console.log('database connected')
}).catch(err => console.log(err))

db.sync({ force: true }).then(() => console.log("synchronization completed"))
    .catch(err => console.log(err.message))

app.use('/', router)


app.listen(3000, (err) => {
    if (err) throw err;
    console.log("Server started");
})







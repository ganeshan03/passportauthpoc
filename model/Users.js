const { Sequelize, DataTypes } = require('sequelize')
const db = require('../dbconnect')

const user = db.define('Users', {
    userid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    isGoogleLogin: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    email_verified: DataTypes.BOOLEAN,
    profileurl: DataTypes.STRING
})

module.exports = user;
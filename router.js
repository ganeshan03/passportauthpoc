const router = require('express').Router();
const controller = require('./controller/authenticate')
const passport = require('passport');

router.get('/', controller.home)
router.get('/signup', controller.getSignup)
router.get('/dashboard', controller.dashboard)
router.post('/signup', controller.userSignup)

router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }))
router.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/' }),
    function (req, res) {
        console.log('redirect')
        res.redirect('/dashboard')
    })
// @desc    Logout user
// @route   /auth/logout
router.get('/logout', (req, res) => {
    // console.log("session1 :" + req.session)
    // req.session = null
    req.logOut()
    res.redirect('/')
    // console.log("session2 :" + req.session)
})

module.exports = router;
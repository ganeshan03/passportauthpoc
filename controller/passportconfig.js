const user = require('../model/Users');
const { compareSync } = require('bcrypt');
var GoogleStrategy = require('passport-google-oauth2').Strategy;

module.exports = function (passport) {
    passport.use(new GoogleStrategy({
        clientID: "256945229420-4ivktf5771bjlsjmi64gb447i28344ve.apps.googleusercontent.com",
        clientSecret: "wRnNP_-QX3wHiC5uKURssvTg",
        callbackURL: "http://localhost:3000/auth/google/callback",
    },

        async function (accessToken, refreshToken, profile, done) {
            user.findOne({ where: { email: profile._json.email } }).then((currentuser) => {
                if (currentuser) {
                    console.log("email already exists")
                    console.log(currentuser)
                    done(null, profile.id);
                } else {
                    console.log("got it inside")
                    console.log(profile)
                    console.log(user.primaryKeyAttribute)
                    try {
                        user.create({
                            userid: profile.id,
                            firstname: profile.given_name,
                            lastname: profile.family_name,
                            email: profile.email,
                            email_verified: profile.email_verified,
                            isGoogleLogin: true,
                            profileurl: profile.picture
                        }).then(newuser => {
                            console.log("before new user")
                            console.log(newuser.isNewRecord)
                            done(null, profile.id)
                        })


                    } catch (error) {
                        console.log(error)
                    }

                }
            })
        }
    ));


    passport.serializeUser(function (userid, done) {
        done(null, userid);
    });

    passport.deserializeUser(function (id, done) {
        user.findByPk(id).then((users) => {
            console.log("inside deserial")
            console.log(users)
            done(null, users)
        }
        ).catch(err => console.log(err))
    });

}
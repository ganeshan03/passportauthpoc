const user = require('../model/Users');
const db = require('../dbconnect');
const bcrypt = require('bcrypt')
const passport = require('passport');
var GoogleStrategy = require('passport-google-oauth2').Strategy;





exports.home = (req, res) => {
    res.render('index');
}
exports.dashboard = (req, res) => {
    res.render('home');
}
exports.getSignup = (req, res) => {
    res.render('signup');
}





exports.userSignup = async (req, res) => {
    const password = await bcrypt.hash(req.body.password, 10);
    user.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: password
    }).then(() => {
        console.log('Sign up completed');
        res.redirect('/')
    }).catch(err => console.log(err.message))
}
